package DataProcessing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

public class RunDataProcessing 
{
	private static String arrayFile = "arrayList_stat.csv";
	private static String hashMapFile = "hashMap_stat.csv";
	private static String treeMapFile = "treeMap_stat.csv";
	
	private static int StartCount = 1000;
	private static int EndCount = 100000;
	private static int CountStepSize = 1000;
	private static int TestRepetition = 100; //Repeat each Test to get better Results. Reduce to lower Number (1-5) to get better Performance.
	
	private static boolean initializeCount = true;
	
	public static void main(String[] args) 
	{
		System.out.println("Stated");
		long startTime = System.nanoTime();
		ArrayList<Measurement> measurements = DoMeasurements(StartCount, EndCount, CountStepSize, TestRepetition);
		
		WriteAllStatistics(measurements);
		long endTime = System.nanoTime();
		
		System.out.println();
		System.out.println("Done in " + (endTime - startTime)/1000000000 + " S");
	}
	
	private static ArrayList<Measurement> DoMeasurements(int startElementCount, int endElementCount, int stepSize, int repetitions)
	{
		ArrayList<Measurement> results = new ArrayList<Measurement>();
		int elementCount = startElementCount;
		while(elementCount <= endElementCount)
		{
			System.out.println(elementCount);
			results.add(DoArrayListMeasurements(elementCount, repetitions, initializeCount));
			results.add(DoHashMapMeasurements(elementCount, repetitions, initializeCount));
			results.add(DoTreeMapMeasurements(elementCount, repetitions));
			elementCount += stepSize;
		}
		
		return results;
	}
	
	private static Measurement DoArrayListMeasurements(int elementCount, int repetitions, boolean initializeCount)
	{
		Measurement result = new Measurement(elementCount, MeasurementType.ArrayList);
		
		String SearchKey = GetTestSearchKey(elementCount);
		
		for (int i = 0; i < repetitions; i++) {
			
			long timeBeforeImport = System.nanoTime();
			ArrayList<Person> persons = GetArrayListData(elementCount, initializeCount);
			long timeAfterImport = System.nanoTime();
			
			Person searchedPerson = null;
			long timeBeforeSearch = System.nanoTime();
			for (Person person : persons) {
				if(person.GetKey().equals(SearchKey))
				{
					searchedPerson = person;
					break;
				}
			}
			long timeAfterSearch = System.nanoTime();
			
			long timeBeforeDelete = System.nanoTime();
			persons.remove(searchedPerson);
			long timeAfterDelete = System.nanoTime();
			
			result.AddInputTimeMeasurement(timeAfterImport - timeBeforeImport);
			result.AddSearchTimeMeasurement(timeAfterSearch - timeBeforeSearch);
			result.AddDeleteTimeMeasurement(timeAfterDelete - timeBeforeDelete);
		}
		
		return result;
	}
	
	private static Measurement DoHashMapMeasurements(int elementCount, int repetitions, boolean initializeCount)
	{
		Measurement result = new Measurement(elementCount, MeasurementType.Hashmap);
		
		String SearchKey = GetTestSearchKey(elementCount);
		
		for (int i = 0; i < repetitions; i++) {
			
			long timeBeforeImport = System.nanoTime();
			HashMap<String,Person> persons = GetHashmap(elementCount, initializeCount);
			long timeAfterImport = System.nanoTime();
			
			Person searchedPerson = null;
			long timeBeforeSearch = System.nanoTime();
			if(persons.containsKey(SearchKey))
				searchedPerson = persons.get(SearchKey);
			long timeAfterSearch = System.nanoTime();
			
			long timeBeforeDelete = System.nanoTime();
			if(persons.containsKey(SearchKey))
				persons.remove(SearchKey);
			long timeAfterDelete = System.nanoTime();
			
			result.AddInputTimeMeasurement(timeAfterImport - timeBeforeImport);
			result.AddSearchTimeMeasurement(timeAfterSearch - timeBeforeSearch);
			result.AddDeleteTimeMeasurement(timeAfterDelete - timeBeforeDelete);
		}
		
		return result;
	}
	
	private static Measurement DoTreeMapMeasurements(int elementCount, int repetitions)
	{
		Measurement result = new Measurement(elementCount, MeasurementType.Treemap);
		
		String SearchKey = GetTestSearchKey(elementCount);
		
		for (int i = 0; i < repetitions; i++) {
			
			long timeBeforeImport = System.nanoTime();
			TreeMap<String,Person> persons = GetTreemap(elementCount);
			long timeAfterImport = System.nanoTime();
			
			Person searchedPerson = null;
			long timeBeforeSearch = System.nanoTime();
			if(persons.containsKey(SearchKey))
				searchedPerson = persons.get(SearchKey);
			long timeAfterSearch = System.nanoTime();
			
			long timeBeforeDelete = System.nanoTime();
			if(persons.containsKey(SearchKey))
				persons.remove(SearchKey);
			long timeAfterDelete = System.nanoTime();
			
			result.AddInputTimeMeasurement(timeAfterImport - timeBeforeImport);
			result.AddSearchTimeMeasurement(timeAfterSearch - timeBeforeSearch);
			result.AddDeleteTimeMeasurement(timeAfterDelete - timeBeforeDelete);
		}
		
		return result;
	}
	
	private static ArrayList<Person> GetArrayListData(int count, boolean initializeCount)
	{
		ArrayList<Person> result;
		if(initializeCount)
			result = new ArrayList<Person>(count);
		else
			result = new ArrayList<Person>();
		
		for (int i = 0; i < count; i++) 
		{
			result.add(GetRandomPerson(i));
		}
		
		return result;
	}
	
	private static HashMap<String, Person> GetHashmap(int count, boolean initializeCount)
	{
		HashMap<String, Person> result;
		
		if(initializeCount)
			result = new HashMap<String, Person>(count);
		else
			result = new HashMap<String, Person>();
		
		for (int i = 0; i < count; i++) {
			Person p =GetRandomPerson(i);
			result.put(p.GetKey(), p);
		}
		
		return result;
	}
	
	private static TreeMap<String, Person> GetTreemap(int count)
	{
		TreeMap<String, Person> result = new TreeMap<String, Person>();
		
		for (int i = 0; i < count; i++) {
			Person p = GetRandomPerson(i);
			result.put(p.GetKey(), p);
		}
		
		return result;
	}
	
	private static Person GetRandomPerson(int index)
	{
		return new Person("Hans" + index,"Huser" +index);
	}

	private static String GetTestSearchKey(int elementCount)
	{
		Person p = GetRandomPerson(elementCount - 1);
		return p.GetKey();
	}
	private static void WriteAllStatistics(ArrayList<Measurement> measurements)
	{
		ArrayList<Measurement> arrayListResults = new ArrayList<Measurement>();
		ArrayList<Measurement> hashMapResults = new ArrayList<Measurement>();
		ArrayList<Measurement> treeMapResults = new ArrayList<Measurement>();
		
		for (Measurement measurement : measurements) {
			if (measurement.GetMeasurementType() == MeasurementType.ArrayList)
			{
				arrayListResults.add(measurement);
			}
			else if(measurement.GetMeasurementType() == MeasurementType.Hashmap)
			{
				hashMapResults.add(measurement);
			}
			else
			{
				treeMapResults.add(measurement);
			}
		}
		
		StatisticsWriter.WriteStatistics(arrayListResults, arrayFile);
		StatisticsWriter.WriteStatistics(hashMapResults, hashMapFile);
		StatisticsWriter.WriteStatistics(treeMapResults, treeMapFile);
	}
}
