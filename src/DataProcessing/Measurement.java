package DataProcessing;

import java.util.ArrayList;
import java.util.Collections;

public class Measurement {
	private int _elementCount;
	private MeasurementType _type;
	private ArrayList<Long> _measuredInputTimes;
	private ArrayList<Long> _measuredSearchTimes;
	private ArrayList<Long> _measuredDeleteTimes;
	
	public Measurement(int elementCount, MeasurementType type)
	{
		Initialize();
		_elementCount = elementCount;
		_type = type;
	}
	
	private void Initialize()
	{
		_measuredInputTimes = new ArrayList<Long>();
		_measuredSearchTimes = new ArrayList<Long>();
		_measuredDeleteTimes = new ArrayList<Long>();
	}
	
	public int GetElementCount()
	{
		return _elementCount;
	}
	public MeasurementType GetMeasurementType()
	{
		return _type;
	}
	public void AddInputTimeMeasurement(long timeInNanoSeconds)
	{
		_measuredInputTimes.add(timeInNanoSeconds);
	}
	public void AddSearchTimeMeasurement(long timeInNanoSeconds)
	{
		_measuredSearchTimes.add(timeInNanoSeconds);
	}
	public void AddDeleteTimeMeasurement(long timeInNanoSeconds)
	{
		_measuredDeleteTimes.add(timeInNanoSeconds);
	}
	
	
	public long GetAverageInputTime()
	{
		return GetAverage(_measuredInputTimes);
	}
	
	public long GetMedianInputTime()
	{
		return GetMedian(_measuredInputTimes);
	}
	
	public long GetAverageSearchTime()
	{
		return GetAverage(_measuredSearchTimes);
	}
	
	public long GetMedianSearchtime()
	{
		return GetMedian(_measuredSearchTimes);
	}
	
	public long GetAverageDeleteTime()
	{
		return GetAverage(_measuredDeleteTimes);
	}
	
	public long GetMedianDeleteTime()
	{
		return GetMedian(_measuredDeleteTimes);
	}
	
	
	
	private long GetAverage(ArrayList<Long> list)
	{
		long aggregate = 0;
		
		for (Long d : list) {
			aggregate += d;
		}
		
		return aggregate / (long)list.size();
	}
	
	private long GetMedian(ArrayList<Long> list)
	{
		int size = list.size();
		if(size>0)
		{
			Collections.sort(list);
			if(size % 2 == 0)
			{
				int mid1 = size/2;
				int mid2 = mid1 -1;
				
				return Math.round(((double)list.get(mid1) + (double)list.get(mid2))/2);
			}
			else
			{
				return list.get(size/2);
			}
		}
		return 0;
	}
}
