package DataProcessing;

public class Person 
{
	private String _firstName;
	private String _lastName;
	
	public Person(String firstName, String lastName)
	{
		_firstName = firstName;
		_lastName = lastName;
	}
	
	public String GetFirstName()
	{
		return _firstName;
	}
	public String GetLastName()
	{
		return _lastName;
	}
	
	public String GetKey()
	{
		return String.format("%s %s", _lastName, _firstName);
	}
}
