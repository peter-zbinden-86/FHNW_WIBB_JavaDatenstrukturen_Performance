package DataProcessing;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class StatisticsWriter 
{
	private static String printPattern = "%s;%s;%s;%s;%s;%s;%s;%s";
	public static void WriteStatistics(ArrayList<Measurement> measurementList, String outputFileName)
	{
		PrintWriter writer = null;
		
		try 
		{
			writer = new PrintWriter(outputFileName, "UTF8");
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}
		
		if(writer != null)
		{
			writer.println(String.format(printPattern,"Typ","Count","Input-Average","Search-Average","Delete-Average","Input-Median","Search-Median","Delete-Median"));
			for (Measurement m : measurementList) 
			{
				writer.println(String.format(printPattern, m.GetMeasurementType(), m.GetElementCount(), m.GetAverageInputTime(), m.GetAverageSearchTime(), m.GetAverageDeleteTime(), m.GetMedianInputTime(), m.GetMedianSearchtime(), m.GetMedianDeleteTime()));
			}
			
			writer.flush();
			writer.close();
		}
	}
}
